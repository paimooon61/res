import urllib.request, zipfile, subprocess, shutil, platform, os.path
import os, requests
from threading import Thread, Lock

xorKey = "0x93"

langToParse = [
    "CHS",
    "EN",
    "JP",
    "KR"
]

numToLang = {
    "01": "CHS",
    "04": "EN",
    "08": "JP",
    "09": "KR"
}

excludeList = {
    "01": ["25863747", "36249376", "27953174"],
    "04": ["32044910", "35718458"],
    "08": ["22498989", "29629080"],
    "09": ["28383130", "33112695"]
}
supportLanguage = {
    "readAble": {
        "CHS": "25863747",
        "CHT": "31408684",
        "DE": "21090035",
        "EN": "35718458",
        "ES": "32578865",
        "FR": "29011584",
        "ID": "25932310",
        "JP": "22498989",
        "KR": "28383130",
        "PT": "33761807",
        "RU": "36373571",
        "TH": "27441594",
        "VI": "28332708",
        "IT": "26638804",
        "TR": "21146056"
    }
}

# asset_studio = "?"
mapped_ai_json = "https://media.githubusercontent.com/media/paimooon/YSAssetIdx/main/output_assetindex_minify.json"

# Function to download a part of the file
def download_chunk(url, start_byte, end_byte, chunk_num, output_file, lock):
    headers = {'Range': f'bytes={start_byte}-{end_byte}'}
    response = requests.get(url, headers=headers, stream=True)
    with lock:
        with open(output_file, 'r+b') as f:
            f.seek(start_byte)
            f.write(response.content)
    print(f"Chunk {chunk_num} downloaded")

# Function to get the file size
def get_file_size(url):
    response = requests.head(url)
    return int(response.headers['Content-Length'])

# Function to download file with multiple threads
def multi_thread_download(url, output_file, num_threads=4):
    file_size = get_file_size(url)
    chunk_size = file_size // num_threads

    # Initialize the output file
    with open(output_file, 'wb') as f:
        f.truncate(file_size)

    threads = []
    lock = Lock()

    for i in range(num_threads):
        start_byte = i * chunk_size
        # Ensure the last chunk goes to the end of the file
        end_byte = start_byte + chunk_size - 1 if i < num_threads - 1 else file_size - 1
        thread = Thread(target=download_chunk, args=(url, start_byte, end_byte, i + 1, output_file, lock))
        threads.append(thread)
        thread.start()

    for thread in threads:
        thread.join()

    print("Download completed")

if __name__ == "__main__":
    print("Res one-click script")
    print("Remember to put blks in Res/blk folder")
    print("")

    shutil.rmtree("./bin/", ignore_errors=True)

    print("Downloading AI json...")
    # multi_thread_download(mapped_ai_json, "ai.json", 16)
    
    subprocess.run(["./studio/AssetStudio.CLI.exe", "./blk/blocks/00/25539185.blk", "./ExcelBinOutput/", "--game", "GI", "--ai_file", "./ai.json", "--types", "MiHoYoBinData", "--key", xorKey])
    shutil.move("./ExcelBinOutput/MiHoYoBinData", "./bin/ExcelBinOutput")
    shutil.rmtree("./ExcelBinOutput/", ignore_errors=True)

    for num in numToLang.keys():
        print(num)
        shutil.rmtree(f"./TextMap_{numToLang[num]}/", ignore_errors=True)

        # List all files in the directory
        files = os.listdir(f'./blk/blocks/{num}')
        # print("Files in directory:", files)
        
        # Check if the specific file exists
        for filename in excludeList[num]:
            for f in files:
                if filename in f:
                    os.remove(f'./blk/blocks/{num}/{filename}.blk')
                    print(f"Deleted file: {filename}")

        subprocess.run(["./studio/AssetStudio.CLI.exe", f"./blk/blocks/{num}/", f"./TextMap_{numToLang[num]}/", "--game", "GI", "--ai_file", "./ai.json", "--types", "MiHoYoBinData", "--key", xorKey])
        shutil.move(f"./TextMap_{numToLang[num]}/MiHoYoBinData", f"./bin/TextMap_{numToLang[num]}")
        shutil.rmtree(f"./TextMap_{numToLang[num]}/", ignore_errors=True)

    print("Done")
    input("Press Enter to continue...")
