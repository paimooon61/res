# Res

A Parser of a certain anime game's data files (WIP)

Currently this repo changed only for blk extraction

Current target version: 4.7 beta 

## Requirements

- Python 3

## How to run

- Clone this repository
```shell
git clone https://gitlab.com/paimooon61/res
```
- Place blk files in `Res/blk`
- Edit textMapLanguage and xorKey in `prepare.py`

- Run prepare
```shell
python prepare.py # only works on windows
```

## BLK info

```
00/24230448 => BinOutput
00/25539185 => ExcelBinOutput (must need)
00/31049740 => Asset Index
00/35323818 => Lua Scripts
01/26692920 => CHS
02/27251172 => CHT
03/25181351 => DE
04/25776943 => EN
05/20618174 => ES
06/25555476 => FR
07/30460104 => ID
08/32244380 => JP
09/22299426 => KR
10/23331191 => PT
11/21030516 => RU
12/32056053 => TH
13/34382464 => VI
14/27270675 => IT
15/21419401 => TR
```
Res currently use only ExcelBinOutput and textmap

## Future Goals

- No

## Credit
- partypooper for the original [KaitaiDumper](https://github.com/partypooperarchive/KaitaiDumper)
- WeedwackerPS for the [DataParser](https://github.com/WeedwackerPS/DataParser)
- Raz for [Studio](https://gitlab.com/RazTools/Studio)
- ToaHartor for [GenshinScripts](https://github.com/ToaHartor/GenshinScripts)
